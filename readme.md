# Android App Take Home Challenge
Street Capital is creating an Android app for busy business folks to track their ToDoList. 
Here are the list of missing features.

**Current Features**

- Long Tap to delete TodoItem
- Tap to edit TodoItem
- Enter new TodoItem


**Missing Features**

- Add Firebase/Parse/Your Own Backend database system to allow users save their current TodoList to the cloud
- Add sign up and login using email and password to Firebase/Parse/Your Own Backend database system. UI design is at your discretion.
- Replace the Long Tap action for deleting an Item with a delete button instead
- Upload your code to GitHub or BitBucket



**Nice to Have**

- Add a simple test framework 
- Add the ability for users to sync their CRUD (Create, Read, Update, Delete) operations to the online cloud database system you are using



